/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.voice;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemProperties;
import android.os.Vibrator;
import android.provider.SearchIndexableResource;
import android.provider.Settings.Global;
import android.provider.Settings.System;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.text.TextUtils;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.Utils;
import com.android.settings.search.BaseSearchIndexProvider;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import static com.android.settings.notification.SettingPref.TYPE_GLOBAL;
import static com.android.settings.notification.SettingPref.TYPE_SYSTEM;

/**
* Add by zzp
* 添加音频输入选项设置界面
**/
public class AudioInputSettings extends SettingsPreferenceFragment implements Preference.OnPreferenceChangeListener {
    private static final String TAG = "AudioInputSettings";


   /*是内置耳麦     0 
     是外置耳麦     1
	 蓝牙耳麦       2 */
    private static final int AUDIO_DEVICE_IN_BUILTIN_MIC = 0;
    private static final int AUDIO_DEVICE_IN_WIRED_HEADSET = 1;
    private static final int AUDIO_DEVICE_IN_BLUETOOTH_SCO_HEADSET = 2;

    private static final String KEY_AUDIO_BUILTIN_MIC = "audio_builtin_mic";
    private static final String KEY_AUDIO_WIRED_HEADSET = "audio_wired_headset";
    private static final String KEY_AUDIO_BLUETOOTH_SCO_HEADSET = "audio_bluetooth_sco_headset";


    // 改变选项，修改系统属性
    private static final String PROPERTY_BOOT_SOUNDS = "persist.sys.mic.switch";

   
    /*private static final SettingPref[] PREFS = {
        KEY_AUDIO_BUILTIN_MIC,
        KEY_AUDIO_WIRED_HEADSET,
        KEY_AUDIO_BLUETOOTH_SCO_HEADSET,
    };*/

    private CheckBoxPreference mBuiltinMic;
    private CheckBoxPreference mWiredHeadset;
    private CheckBoxPreference mBluetoothHeadset;

    //private final SettingsObserver mSettingsObserver = new SettingsObserver();

    private Context mContext;

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.AUDIO_INPUT;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.audio_input_settings);

        mContext = getActivity();

        /*for (SettingPref pref : PREFS) {
            pref.init(this);
        }*/


		mBuiltinMic = (CheckBoxPreference)getPreferenceScreen().findPreference(KEY_AUDIO_BUILTIN_MIC);
		mWiredHeadset = (CheckBoxPreference)getPreferenceScreen().findPreference(KEY_AUDIO_WIRED_HEADSET);
		mBluetoothHeadset = (CheckBoxPreference)getPreferenceScreen().findPreference(KEY_AUDIO_BLUETOOTH_SCO_HEADSET);

		mBuiltinMic.setOnPreferenceChangeListener(this);
		mWiredHeadset.setOnPreferenceChangeListener(this);
		mBluetoothHeadset.setOnPreferenceChangeListener(this);
		
	// 暂不支持蓝牙耳机，如果支持直接屏蔽下边一行
	getPreferenceScreen().removePreference(mBluetoothHeadset);

    }

    @Override
    public void onResume() {
        super.onResume();
        //mSettingsObserver.register(true);
		initData();
    }

    @Override
    public void onPause() {
        super.onPause();
        //mSettingsObserver.register(false);
    }

	//　初始化选项(默认是自动保存的，但项目比较紧，暂时这样处理)
	private void initData(){
		String micInputStr = SystemProperties.get(PROPERTY_BOOT_SOUNDS);
		int micInputNum = 0;		
		if(!TextUtils.isEmpty(micInputStr)) {
			micInputNum = Integer.parseInt(micInputStr);
		}

		mBuiltinMic.setChecked(false);
		mWiredHeadset.setChecked(false);
		mBluetoothHeadset.setChecked(false);

		if(micInputNum == AUDIO_DEVICE_IN_WIRED_HEADSET) {
			mWiredHeadset.setChecked(true);
		}else if (micInputNum == AUDIO_DEVICE_IN_BLUETOOTH_SCO_HEADSET) {
			mBluetoothHeadset.setChecked(true);
		}else {
			mBuiltinMic.setChecked(true);
		}
	}

	@Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (mBuiltinMic != null && mBuiltinMic == preference) {         			
			SystemProperties.set(PROPERTY_BOOT_SOUNDS, "0");
            return true;
        } else if (mWiredHeadset != null && mWiredHeadset == preference) {           			
			SystemProperties.set(PROPERTY_BOOT_SOUNDS, "1");
			return true;
        } else if(mBluetoothHeadset != null && mBluetoothHeadset == preference) {  		
			SystemProperties.set(PROPERTY_BOOT_SOUNDS, "2");
			return true;
		}
        return false;
    }



    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
		initData();
		Log.d("ZZP",">>>> onPreferenceTreeClick : " + SystemProperties.get(PROPERTY_BOOT_SOUNDS));
		/*mBuiltinMic.setChecked(false);
		mWiredHeadset.setChecked(false);
		mBluetoothHeadset.setChecked(false);

        if (mBuiltinMic != null && preference == mBuiltinMic) {
			mBuiltinMic.setChecked(true);
            SystemProperties.set(PROPERTY_BOOT_SOUNDS, "0");
            return false;
        } else if (mWiredHeadset != null && preference == mWiredHeadset) {
			mWiredHeadset.setChecked(true);
			SystemProperties.set(PROPERTY_BOOT_SOUNDS, "1");
 			return false;
		} else if (mBluetoothHeadset != null && preference == mBluetoothHeadset) {
			mBluetoothHeadset.setChecked(true);
			SystemProperties.set(PROPERTY_BOOT_SOUNDS, "2");
 			return false;
		} else {
            return super.onPreferenceTreeClick(preference);
        }*/
		return super.onPreferenceTreeClick(preference);
    }

  

    // === Callbacks ===

    /*private final class SettingsObserver extends ContentObserver {
        public SettingsObserver() {
            super(new Handler());
        }

        public void register(boolean register) {
            final ContentResolver cr = getContentResolver();
            if (register) {
                for (SettingPref pref : PREFS) {
                    cr.registerContentObserver(pref.getUri(), false, this);
                }
            } else {
                cr.unregisterContentObserver(this);
            }
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
            for (SettingPref pref : PREFS) {
                if (pref.getUri().equals(uri)) {
                    pref.update(mContext);
                    return;
                }
            }
        }
    }*/

  
}
