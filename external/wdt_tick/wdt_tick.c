/*
 * Watchdog Driver Test Program
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/watchdog.h>

#define PING_INTERVAL_SECS  15
int fd;

/*
 * This function simply sends an IOCTL to the driver, which in turn ticks
 * the PC Watchdog card to reset its internal timer so it doesn't trigger
 * a computer reset.
 */
static void keep_alive(void)
{
    int dummy;
    ioctl(fd, WDIOC_KEEPALIVE, &dummy);
}

/*
 * The main program.  Run the program with "-d" to disable the card,
 * or "-e" to enable the card.
 */

static void term(int sig)
{
    close(fd);
    fprintf(stderr, "Stopping watchdog ticks...\n");
    exit(0);
}

int main(int argc, char *argv[])
{
    int flags;
    unsigned int ping_rate;

    fd = open("/dev/watchdog", O_WRONLY);

    if (fd == -1) {
	fprintf(stderr, "Watchdog device not enabled.\n");
	fflush(stderr);
	perror("open failed");
	exit(-1);
    }

	ping_rate = PING_INTERVAL_SECS;
	fprintf(stderr, "Watchdog ping rate set to %u seconds.\n", ping_rate);
	fflush(stderr);
    fprintf(stderr, "Watchdog Ticking Away!\n");
    fflush(stderr);

    signal(SIGINT, term);

    while(1) {
	keep_alive();
	sleep(ping_rate);
    }
end:
    close(fd);
    return 0;
}
